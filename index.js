const { promises: fsPromises } = require("fs")
const AWS = require("aws-sdk")
const mime = require("mime-types")
const glob = require("glob")

async function deploy({ accessKey, secretKey, bucket, targetPath = "", distPath = "" } = {}) {
  if (!accessKey || !secretKey) {
    throw new Error("cannot upload S3")
  }

  const s3 = new AWS.S3({
    accessKeyId: accessKey,
    secretAccessKey: secretKey,
    signatureVersion: "v4",
    region: "eu-west-3"
  })

  const files = await listAll(distPath)

  for (const file of files) {
    const stat = await fsPromises.lstat(file)
    const mimeType = mime.lookup(file)
    if (mimeType && stat.isFile()) {
      const data = await fsPromises.readFile(file)
      const headers = {}

      if (mimeType === "text/html") {
        Object.assign(headers, {
          CacheControl: "public, max-age=0, must-revalidate",
          ContentType: "text/html; charset=utf-8"
        })
      } else {
        Object.assign(headers, {
          CacheControl: "public, max-age=31536000, immutable",
          ContentType: mimeType
        })
      }

      const params = {
        Body: data,
        Bucket: bucket,
        ...headers,
        ACL: "public-read",
        Key: `${targetPath}${file.slice(distPath.length + 1)}`
      }

      await s3.putObject(params).promise()
    }
  }
  process.exit()
}

function listAll(dir) {
  return new Promise((resolve, reject) => {
    glob(dir + "/**/*", function (err, files) {
      if (err) {
        reject(err)
      } else {
        resolve(files)
      }
    })
  })
}

module.exports = deploy
